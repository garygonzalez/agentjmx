package hn.com.tigo.ne.controller;

import hn.com.tigo.ne.common.GeneralResponse;
import hn.com.tigo.ne.common.controller.EventDBController;
import hn.com.tigo.ne.common.converter.json.JSONConverter;
import hn.com.tigo.ne.entity.NEFault;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BBSaleNotBBPosDBClient extends EventDBController {

    public BBSaleNotBBPosDBClient() {
        super();
    }

    public BBSaleNotBBPosDBClient(DataSource ds) {
        super(ds);
    }

    private static final Log LOGGER = LogFactory.getFactory().getInstance(BBSaleNotBBPosDBClient.class);

    @Override
    public void initClass() {
    }

    @Override
    public GeneralResponse invoke(Object input) {

        GeneralResponse generalResponse = new GeneralResponse(-1, null);
        NEFault nEFault = (NEFault) input;
        String jsonEvent = new JSONConverter().convertTo(nEFault.getEvent());
        
        String sql =
            "INSERT INTO LECTURABBAS_FALLIDA (QUEUE_DETAIL, ERROR) " + "VALUES ('" +
            jsonEvent.replaceAll("'", "\"") + "', '" + (nEFault.getFault()!=null? nEFault.getFault().replaceAll("'", "\"") :"") + "') ";

        try {
            con = dataSource.getConnection();
            stmt = con.createStatement();
            int result = stmt.executeUpdate(sql);
            LOGGER.info("Insert to LecturaBBAS_Fallida OK :: " + result);

            generalResponse.setCode(0);
            generalResponse.setMessage("Insert to LecturaBBAS_Fallida OK!");

        } catch (SQLException e) {
            LOGGER.error("SQLException :: " + e.getMessage() + " - SQL :: " + sql, e);
        } finally {
            destroy(con, stmt);
        }
        return generalResponse;
    }
}
