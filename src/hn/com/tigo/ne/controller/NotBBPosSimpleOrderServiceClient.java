package hn.com.tigo.ne.controller;

import com.tigo.josm.gateway.services.order.additionalparameterdto.v1.AdditionalParameters;
import com.tigo.josm.gateway.services.order.simpleorderrequest.v1.SimpleOrderRequest;

import hn.com.tigo.ne.client.ws.ExecuteOrderSimpleOrderController;
import hn.com.tigo.ne.common.GeneralResponse;
import hn.com.tigo.ne.common.util.ConfigurationLoader;
import hn.com.tigo.ne.event.BBSaleEvent;
import hn.com.tigo.ne.util.PropertiesLoader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class NotBBPosSimpleOrderServiceClient extends ExecuteOrderSimpleOrderController {

    public NotBBPosSimpleOrderServiceClient() {
        super();
    }
    
    private static final Log LOGGER = LogFactory.getFactory().getInstance(NotBBPosSimpleOrderServiceClient.class);
    protected ConfigurationLoader propertiesLoader;

    @Override
    public void initClass() {
        propertiesLoader = PropertiesLoader.getInstance();
        this.endPointService = propertiesLoader.getProperty(PropertiesLoader.PROPERTIES_RESOURCE, PropertiesLoader.EXECUTE_ORDER);
        super.initClass();        
    }

    @Override
    public GeneralResponse invoke(Object input) {

        GeneralResponse generalResponse = new GeneralResponse(-1, null);
        BBSaleEvent event = (BBSaleEvent)input;
        
        SimpleOrderRequest simpleOrderRequest = new SimpleOrderRequest();
        simpleOrderRequest.setChannelId(2);
        simpleOrderRequest.setSubscriberId(event.getSubscriberId());
        simpleOrderRequest.setProductId(130);
        simpleOrderRequest.setQuantity(1);
        simpleOrderRequest.setExternalTransacionId("1");
        simpleOrderRequest.setComment("1");
        
        AdditionalParameters additionalParameters = setParameterValue(null, "origen_bb", "1");
        setParameterValue(additionalParameters, "origen_bb", "1");
        setParameterValue(additionalParameters, "anexo", event.getAnnex());
        setParameterValue(additionalParameters, "planid", event.getCodePlan());        
        setParameterValue(additionalParameters, "imsianterior", event.obteinValueParam("IMSI anterior"));
        setParameterValue(additionalParameters, "imsinueva", event.obteinValueParam("IMSI nueva"));
        setParameterValue(additionalParameters, "fechaalta", event.getHighDate());
        setParameterValue(additionalParameters, "fechabaja", event.getDownDate());
        setParameterValue(additionalParameters, "fechanotificacion", event.getNotificationDate());
        setParameterValue(additionalParameters, "accion", event.getAction());
        setParameterValue(additionalParameters, "programa", event.getProcess());
        
        simpleOrderRequest.setAdditionalParameters(additionalParameters);

        generalResponse = super.invoke(simpleOrderRequest);
        return generalResponse;
    }
}