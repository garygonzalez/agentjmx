package hn.com.tigo.ne.outbound;

import com.bea.wlevs.ede.api.EventRejectedException;
import com.bea.wlevs.ede.api.StreamSink;

import hn.com.tigo.ne.common.GeneralResponse;
import hn.com.tigo.ne.controller.BBSaleNotBBPosDBClient;
import hn.com.tigo.ne.controller.NotBBPosSimpleOrderServiceClient;
import hn.com.tigo.ne.entity.NEFault;
import hn.com.tigo.ne.event.Event;

import javax.annotation.Resource;

import javax.sql.DataSource;

public class NotBBSaleOutbound implements StreamSink {

    public NotBBSaleOutbound() {
        super();
    }

    @Override
    public void onInsertEvent(Object object) throws EventRejectedException {

        NotBBPosSimpleOrderServiceClient service = new NotBBPosSimpleOrderServiceClient();
        GeneralResponse generalResponse = service.invoke(object);
        if (generalResponse.getCode() != 0) {            
            NEFault nEFault = new NEFault((Event) object, generalResponse.getMessage());
            BBSaleNotBBPosDBClient controller = new BBSaleNotBBPosDBClient(dataSource);
            controller.invoke(nEFault);
        }
    }
    

    protected DataSource dataSource = null;

    @Resource(name = "ds_TIGO_JOSM")
    public void setDataSource(DataSource ds) {
        this.dataSource = ds;
    }
}
