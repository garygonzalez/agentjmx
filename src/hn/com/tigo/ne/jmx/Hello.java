package hn.com.tigo.ne.jmx;

public class Hello extends AbstractOCEPClass implements HelloMBean {
    
    private String _msg;

    /**
     * @param message
     */
    public Hello(String message) {
        setMessage(message);        
    }

    @Override
    public void sayHello() {
        // TODO Implement this method
        System.out.println("Hello " + getMessage());
    }

    @Override
    public String getMessage() {
        // TODO Implement this method
        return this._msg;
    }

    @Override
    public void setMessage(String message) {
        // TODO Implement this method
        this._msg=message;
    }    
}
