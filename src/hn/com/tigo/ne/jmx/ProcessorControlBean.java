package hn.com.tigo.ne.jmx;

import com.bea.wlevs.ede.api.ActivatableBean;
import com.bea.wlevs.management.configuration.EPLProcessorMBean;
import java.io.IOException;
import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.JMException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class ProcessorControlBean implements ActivatableBean{
   
    private EPLProcessorMBean processor;   
        
    public void setProcessor(EPLProcessorMBean processor) {

        this.processor = processor;

    }
    
     /**
     * @param hostname
     * @param port
     * @param username
     * @param password
     * @throws IOException
     * @throws MalformedURLException
     * @throws MalformedObjectNameException
     * @throws JMException
     * @author Gary Gonzalez Zepeda
     */
    public static void initConnection(String hostname, int port, String username, char[] password)
               throws IOException, MalformedURLException, MalformedObjectNameException, JMException {

           Map<String, Object> env = new HashMap<String, Object>();

          env.put(JMXConnector.CREDENTIALS, new Serializable[]{username, password});
                  env.put("jmx.remote.authenticator", "com.bea.core.jmx.server.CEAuthenticator");
                  System.setProperty("jmx.remote.authenticator", "com.bea.core.jmx.server.CEAuthenticator");
                    env.put(JMXConnectorFactory.DEFAULT_CLASS_LOADER,
                                        com.bea.core.jmx.remote.provider.msarmi.ServerProvider.class
                                        .getClassLoader());
                    env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_CLASS_LOADER,
                                        com.bea.core.jmx.remote.provider.msarmi.ServerProvider.class
                                        .getClassLoader());

                    env.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES,
                                        "com.bea.core.jmx.remote.provider");

                    System.setProperty("mx4j.remote.resolver.pkgs",
                                        "com.bea.core.jmx.remote.resolver");
                  env.put("jmx.remote.protocol.provider.pkgs", "com.bea.core.jmx.remote.provider");
                  env.put("mx4j.remote.resolver.pkgs", "com.bea.core.jmx.remote.resolver");
                  env.put("java.naming.factory.initial", "com.bea.core.jndi.context.ContextFactory");  

                  JMXServiceURL serviceUrl = new JMXServiceURL(
                          "MSARMI", hostname, port, "/jndi/jmxconnector"
                  );
                  /*System.out.println("Service: " + serviceUrl.toString());

                  JMXConnector connector = JMXConnectorFactory.connect(serviceUrl, env);

                  MBeanServerConnection connection = connector.getMBeanServerConnection();*/
                                    
              }


    /**
     * @param objectName
     * @throws MalformedObjectNameException
     * @throws InstanceAlreadyExistsException
     * @throws MBeanRegistrationException
     * @throws NotCompliantMBeanException
     * @author Gary Gonzalez Zepeda
     */
    public static void MbeanCreator(String objectName) throws MalformedObjectNameException, InstanceNotFoundException,
                                                       MBeanRegistrationException,
                                                       NotCompliantMBeanException {
                               
           MBeanServer server = ManagementFactory.getPlatformMBeanServer();
           ObjectName mbeanName = new ObjectName(objectName);
           
           /*Instanciar clase */              
           Hello mbean = new Hello("Gary");
        try {
            server.registerMBean(mbean, mbeanName);   
            Set<ObjectInstance> instances;
            instances = server.queryMBeans(new ObjectName(objectName), null);
            ObjectInstance instance;
            instance = (ObjectInstance) instances.toArray()[0];            
        } catch (InstanceAlreadyExistsException e) {
            server.unregisterMBean(mbeanName);
            try {
                server.registerMBean(mbean, mbeanName);
                Set<ObjectInstance> instances;
                instances = server.queryMBeans(new ObjectName(objectName), null);
                ObjectInstance instance;
                instance = (ObjectInstance) instances.toArray()[0];                
            } catch (InstanceAlreadyExistsException f) {
                System.out.println(f.getLocalizedMessage());
            }
        }
       
    }
        
    /**
     * @throws Exception
     */
    @Override

    public void afterConfigurationActive() throws Exception {

        initConnection("localhost", 9002, "wlevs", new char[]{'w', 'e', 'l', 'c', 'o','m','e','1'});      
       
        MbeanCreator("hn.com.tigo.ne.jmx:type=Hello");  
    }           
   
}


