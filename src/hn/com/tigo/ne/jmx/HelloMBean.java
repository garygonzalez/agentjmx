package hn.com.tigo.ne.jmx;

public interface HelloMBean extends AbstractJMXOCEP{
    // operations
                    
   public void sayHello();

    // attributes
                    
    // a read-write attribute called Message of type String

    /**
     * @return
     */
    public String getMessage();

    /**
     * @param message
     */
    public void setMessage(String message);
}
