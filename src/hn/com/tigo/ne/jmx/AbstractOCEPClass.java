package hn.com.tigo.ne.jmx;

import java.util.concurrent.atomic.AtomicLong;

import javax.ejb.Schedule;

public abstract class AbstractOCEPClass implements AbstractJMXOCEP{
    
    /** Attribute that determine a Constant of INIT_VALUE. */
            protected static final long INIT_VALUE = 0L; 
            
            /** Contains inboundMessages metric value. */
            private final AtomicLong _inboundMessages;

            /** Contains lastInboundMessages metric value. */
            private final AtomicLong _lastInboundMessages;

            /** Contains failedMessages metric value. */
            private final AtomicLong _failedMessages;

            /** Contains lastTransactionTimeMillis metric value. */
            private final AtomicLong _lastTransactionTimeMillis;
            
            /** Contains tps metric value. */
            private AtomicLong _tps;
            
            
            /**
             * Instantiates a new JOSM monitoring.
             */
            public AbstractOCEPClass(){
                    _inboundMessages = new AtomicLong(INIT_VALUE);
                    _failedMessages = new AtomicLong(INIT_VALUE);
                    _lastTransactionTimeMillis = new AtomicLong(INIT_VALUE);
                    _lastInboundMessages = new AtomicLong(INIT_VALUE);
                    _tps = new AtomicLong(INIT_VALUE);
            }
          
            @Override
            public long getInboundMessages() {
                    return _inboundMessages.longValue();
            }

            @Override
            public long getFailedMessages() {
                    return _failedMessages.longValue();
            }

            @Override
            public long getSucessMessages() {
                    return _inboundMessages.longValue()
                                    - _failedMessages.longValue();
            }
            
            @Override
            public void incrementInboundMessages() {
                    this._inboundMessages.getAndIncrement();
            }

            
            @Override
            public void incrementFailedMessages() {
                    this._failedMessages.getAndIncrement();
                    this._inboundMessages.getAndIncrement();
                    
            }

            @Override
            public long getLastTransactionTimeMillis() {
                    return _lastTransactionTimeMillis.longValue();
            }

           
            @Override
            public void setLastTransactionTimeMillis(long lastTransactionTimeMillis) {
                    this._lastTransactionTimeMillis.set(lastTransactionTimeMillis);
            }

          
            @Override
            public long getTPS() {
                    return _tps.longValue();
            }
            
            
            @Override
            public void calculateTPS() {
                    _tps.set(_inboundMessages.longValue()
                                    - _lastInboundMessages.longValue());
                    _lastInboundMessages.set(_inboundMessages.intValue());
            }

            @Override
            public void reset() {
                    _inboundMessages.set(INIT_VALUE);
                    _failedMessages.set(INIT_VALUE);
                    _lastTransactionTimeMillis.set(INIT_VALUE);
                    _lastInboundMessages.set(INIT_VALUE);
                    _tps.set(INIT_VALUE);
            }

}
