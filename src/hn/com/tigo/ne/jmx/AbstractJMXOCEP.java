package hn.com.tigo.ne.jmx;

public interface AbstractJMXOCEP {

    /**
     * Gets the inbound messages.
     *
     * @return the inbound messages
     */
    long getInboundMessages();

    /**
     * Gets the failed messages.
     *
     * @return the failed messages
     */
    long getFailedMessages();

    /**
     * Gets the sucess messages.
     *
     * @return the sucess messages
     */
    long getSucessMessages();

    /**
     * Increment inbound messages.
     */
    void incrementInboundMessages();

    /**
     * Increment failed messages.
     */
    void incrementFailedMessages();

    /**
     * Gets the last transaction time millis.
     *
     * @return the last transaction time millis
     */
    long getLastTransactionTimeMillis();

    /**
     * Sets the last transaction time millis.
     *
     * @param lastTransactionTimeMillis
     *            the new last transaction time millis
     */
    void setLastTransactionTimeMillis(long lastTransactionTimeMillis);

    /**
     * Gets the tps.
     *
     * @return the tps
     */
    long getTPS();

    /**
     * Calculate tps.
     */
    void calculateTPS();
    
    /**
     * Reset monitoring variables for JMX service.
     */
    void reset();
}
