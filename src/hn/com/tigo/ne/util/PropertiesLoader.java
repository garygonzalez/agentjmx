package hn.com.tigo.ne.util;

import hn.com.tigo.ne.common.util.ConfigurationLoader;

/**
 * Carga la configuracion de las propiedades de la aplicacion.
 * @author diego fagua
 */
public class PropertiesLoader extends ConfigurationLoader{
    
    //public static final String AGENT_NAME = "agentBBSaleNotBBPos";
    public static final int AGENT_ID = 1;

    private static final String WS_URL_RESOURCE = ".url";
    public static final String EXECUTE_ORDER = "executeOrder.ws" + WS_URL_RESOURCE;   
    
    public static final String PROPERTIES_RESOURCE = "properties/ne.agentBBSaleNotBBPos.properties";
    
    private static PropertiesLoader loader = new PropertiesLoader();
    
    private PropertiesLoader(){       
    }
    
    public static PropertiesLoader getInstance(){
        return loader;
    }       
}
